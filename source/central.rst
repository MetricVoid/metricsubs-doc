.. role:: strike
    :class: strike

MetricSubs字幕组指南 中心页
===========================

欢迎来到MetricSubs字幕组的中心页！请阅读以下提示，并在以下栏目中选取您想查阅的内容

本指南中有许多内容是通用的，但是也有许多是本组特有。如果是从其他组过来的，请着重阅读特有部分。
如果之后要转到其他组，请和他们的组长/成员确认工作流程是否相同。

特有内容会有这样的标志：

.. note::
   此内容为MetricSubs特有。在他组时请多确认

使用软件
--------
- `Python：Python语言的运行环境`_
- `Youtube-DL：用于从youtube下载视频`_
- :strike:`git/SmartGit：字幕文件仓库、版本管理`
- :strike:`自建网盘：文件仓库`
- `Aegisub：字幕文件编辑`_
- `ffmpeg：压制视频用的命令行`_
- `video-packer：压制视频所用的图形界面，可以比较方便地切广告。需要ffmpeg`_


使用平台
--------
- :strike:`GitLab：字幕版本管理/托管`
- :strike:`自建网盘`
- `Makeflow`_
- `资源平台(境内无梯)`_
- `资源平台(境外直连)`_  <-- 如果您有梯子，推荐使用这个

.. _`Python：Python语言的运行环境`: softwares/python.html
.. _`Youtube-DL：用于从youtube下载视频`: softwares/youtubedl.html
.. _`git/SmartGit：字幕文件仓库、版本管理`: softwares/git.html
.. _`自建网盘：文件仓库`: softwares/netdrive.html
.. _`Aegisub：字幕文件编辑`: softwares/aegisub.html
.. _`ffmpeg：压制视频用的命令行`: softwares/ffmpeg.html
.. _`video-packer：压制视频所用的图形界面，可以比较方便地切广告。需要ffmpeg`: softwares/video-packer.html
.. _`GitLab：字幕版本管理/托管`: https://gitlab.com
.. _`自建网盘`: https://repo.metricsubs.org
.. _`Makeflow`: https://makeflow.com]
.. _`资源平台(境内无梯)`: https://res.metricsubs.org/
.. _`资源平台(境外直连)`: https://res-direct.metricsubs.org/