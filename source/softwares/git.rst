Git
===

.. warning::
    除非您在进行字幕组软件的开发，否则不需要用到此平台。

git是一个版本管理工具。它可以追踪文件的变化、比较不同版本、回退变化、以及找出做出某个编辑的人。
同时还有类似GitHub和GitLab的平台，可以在互联网上托管git仓库。

.. contents::

下载、安装
----------
前往 `git-scm`_ 网站，下载git的最新版本安装包

.. note::
   以下内容为MetricSubs特有。在他组时请多确认

安装过程中使用默认参数，除了以下选项

.. image:: ../_static/images/git-editor.png
    
此处选择你喜欢的编辑器。不会vim的话千万别选。
如果没有安装其他编辑器的话可以选nano。这也是一个命令行文字编辑器，但是界面比较友好。
我们一般用不到编辑器，除非你在命令行忘了打 :code:`-m`

.. image:: ../_static/images/git-pull-options.png
    
此处使用rebase是习惯，让分支少一点。

安装完成后最好先设置自己的用户名和邮箱。
    设定自己的用户名。使用 :code:`git config --global user.name [你的名字]` 设定你的用户名。用户名可以自选，不需要和GitHub/GitLab一致。这只需要做一次。
    然后设定邮箱。使用 :code:`git config --global user.email [邮箱]` 设定你的邮箱。这也可以自选，不需要和GitHub/GitLab一致。这只需要做一次。

使用（命令行）
--------------
**git的机制和命令行操作并不是很好理解。在日常使用中为了避免把仓库搞乱，还是推荐使用图形界面，但阅读命令行方法有助于了解机制。**

以下是git的命令行方法。

git仓库由一串commit组成。每个commit包含文件的变化情况。变化有三种模式(mode)，分别是create mode（新文件）、change mode (文件发生变化) 和delete mode（文件被删除）。
commit构成一个树形结构，称为一个branch。在树的底端是仓库的首个(最古老的)commit，顶端是最新的commit，也被称为HEAD。仓库中的文件和HEAD对应。当仓库被修改时，git会发现它和HEAD不同，并将变化整理出来。

.. note::
    commit既是名词也是动词。做动词时表达创建commit的操作。

操作本地仓库
~~~~~~~~~~~~
要创建一个仓库，使用 :code:`git init` 。
    创建的新仓库中没有任何的branch和commit。创建新commit之后，会自动创建新的master branch。

要获取一个远端仓库，使用 :code:`git clone` 。
    这会将远程仓库克隆到本地。

要查看一个仓库的情况，使用 :code:`git status` 。
    这会对比HEAD和现在仓库，并列出发生变化的文件。发生变化的文件以红色显示。

文件变化之后，需要将其封装为commit才能将其记录下来，并提交到远程仓库。git创建commit的方式是将暂存区(staging area)的文件和HEAD对比，比较差别，并封装为commit。

为此，我们需要先将文件加入暂存区。
    使用 :code:`git add [文件名]` 来将文件复制到暂存区。 :code:`[文件名]` 中可以使用通配符，例如 :code:`*` 。 :code:`git add *` 会将当前目录下的所有文件都加入暂存区。
    暂存区中的文件在 :code:`git status` 中会以绿色显示。

暂存区存储了文件内容，而不仅仅是文件名。因此如果该文件再次发生变化，它仍然会被git标记。在 :code:`git status` 中，绿色和红色的文件名会同时出现。此时使用 :code:`git add` 可以覆盖暂存区中的文件。
如果再次不使用 :code:`git add` 的话，commit时会commit旧版本，即最后一次 :code:`git add` 时的版本。

在确认暂存区中内容无误后，可以开始准备commit了。不过每一个commit都必须有三个元数据：作者、作者邮箱、以及commit信息。
    如果安装时已经设定了用户名和邮箱的话，以下两行可以忽略。
    首先设定自己的名字。使用 :code:`git config --global user.name [你的名字]` 设定你的用户名。用户名可以自选，不需要和GitHub/GitLab一致。这只需要做一次。
    然后设定邮箱。使用 :code:`git config --global user.email [邮箱]` 设定你的邮箱。这也可以自选，不需要和GitHub/GitLab一致。这只需要做一次。

在用户名和邮箱设定完毕之后，使用 :code:`git commit -m [信息]` 来创建commit。如果没有指定 :code:`-m` 参数的话，git会打开文本编辑器，让你输入一个信息。

此外还有一个操作 :code:`git stash`。这个指令会将所有本地发生变化的文件暂存起来（不是add的暂存），并将仓库文件内容重置到HEAD。使用 :code:`git apply stash` 可以将stash的暂存区重新应用到仓库上。

操作远程仓库
~~~~~~~~~~~~
使用 :code:`git clone [URL]` 会创建一个新的文件夹，并将远程仓库克隆进那个文件夹。

使用 :code:`git fetch` 会将远程仓库中的commit拉到本地，但暂时并不将这些commit应用到本地仓库上。

使用 :code:`git pull` 会将远程仓库中的commit拉到本地，并应用到当前仓库上。本地有文件发生变化，但没有commit时无法进行此操作，需要stash。

使用 :code:`git push` 会将本地仓库中的commit发送到远程仓库。

工作流程
~~~~~~~~
.. note::
   以下内容为MetricSubs特有。在他组时请多确认
   
入组后先 :code:`git clone` 一下，将远程仓库下载到本地。在每次开始工作前，使用 :code:`git pull` 获取远程仓库的最新变化。
完成工作之后，使用 :code:`git add` 和 :code:`git commit` 创建commit，并使用 :code:`git push` 发送到远程仓库。

使用（图形界面）
----------------
git有许多图形界面，比如SmartGit和SourceTree。SmartGit相比SourceTree更加智能，可以自动解决stash的问题。
SourceTree是完全免费的软件，而SmartGit只有非商用的是免费，每次启动之前需要等30秒。
更有经验的可以使用SourceTree，经验略少的可以使用SmartGit。

- SourceTree的配置方法：SourceTree_
- SmartGit的配安置方法：SmartGit_


.. _`git-scm`: https://git-scm.com/downloads
.. _SourceTree: sourcetree.html
.. _SmartGit: smartgit.html
