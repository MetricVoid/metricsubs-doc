Python
======

.. important::
    | Python是非常热门、快速更新的语言。本页面上的内容可能不是最新。
    | 一般情况下使用最新版本Python即可。

该段介绍如何配置Python环境，以使用各种基于Python的工具。


如果你已熟悉Python
------------------
如果你对Python很熟悉或者已经安装了Python/Anaconda，可以直接使用它。
只要确认Python以及Python\Scripts在PATH中即可。
Anaconda可能需要使用开始菜单中的Anaconda Prompt。在Mac/Linux下，需要 :code:`activate [environment]` 。


如果你不熟悉Python
------------------
首先，前往 Python官网_ 下载最新版本的Python。在安装时，注意勾选以下几个选项（使用Python 3.8示例）

.. TODO::
    等我装好虚拟机截图


之后打开命令行，输入 :code:`python --version`，看到版本号即表示正确安装完毕。

..  _Python官网: https://www.python.org/downloads/